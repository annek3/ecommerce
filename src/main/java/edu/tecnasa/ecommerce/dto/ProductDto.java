package edu.tecnasa.ecommerce.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.tecnasa.ecommerce.entities.Category;

public class ProductDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Id")
	private Long id;
	
	@JsonProperty("Title")
	private String title;
	
	@JsonProperty("Price")
	private BigDecimal price;
	
	@JsonProperty("IsSpecial")
	private boolean special;
	
	private Category category;
	
	@JsonProperty("Desc")
	private String descripcion;
	
	@JsonProperty("ImageS")
	private String imageS;
	
	@JsonProperty("ImageL")
	private String imageL;
	
	
	public ProductDto() {
		
	}

	public ProductDto(Long id, String title, BigDecimal price, boolean special,String descripcion, Category category, 
			String imageS, String imageL) {
		
		this.id = id;
		this.title = title;
		this.price = price;
		this.special = special;
		this.descripcion = descripcion;
		this.category = category;
		
		this.imageS = imageS;
		this.imageL = imageL;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isSpecial() {
		return special;
	}

	public void setSpecial(boolean special) {
		this.special = special;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImageS() {
		return imageS;
	}

	public void setImageS(String imageS) {
		this.imageS = imageS;
	}

	public String getImageL() {
		return imageL;
	}

	public void setImageL(String imageL) {
		this.imageL = imageL;
	}
	
	
}
