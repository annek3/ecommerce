package edu.tecnasa.ecommerce.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Id")
	private Long id;
	
	@JsonProperty("Title")
	private String title;
	
	@JsonProperty("Desc")
	private String descripcion;
	
	@JsonProperty("ImageS")
	private String imageS;
	
	@JsonProperty("ImageL")
	private String imageL;

	public CategoryDto() {
		
	}
	
	public CategoryDto(Long id, String title, String descripcion, String images, String imageL) {
		super();
		this.id = id;
		this.title = title;
		this.descripcion = descripcion;
		this.imageS = images;
		this.imageL = imageL;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImages() {
		return imageS;
	}

	public void setImages(String images) {
		this.imageS = images;
	}

	public String getImageL() {
		return imageL;
	}

	public void setImageL(String imageL) {
		this.imageL = imageL;
	}
	
	
	
	
	
}
